﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer
{
	public delegate void TimerDelegate();
	public delegate void TimeElapsedDelegate(int time);

	public TimerDelegate onTimerEnded;
	public TimeElapsedDelegate onSecondElapsed;
	public TimeElapsedDelegate onMinuteElapsed;

	bool enabled = false;

	public float maxTime = 1;
	public float timeLeft = 0;
	public bool loop = false;

	// Time elapsed tracking
	int second = 0;
	int minute = 0;

	public Timer()
	{
		timeLeft = maxTime;
	}

	public void Update(float ElapsedTime = 0)
	{
		if (!enabled) return;

		if (ElapsedTime == 0)
		{
			ElapsedTime = Time.deltaTime;
		}
		timeLeft -= ElapsedTime;

		if (timeLeft <= 0)
		{
			if (loop)
			{
				timeLeft = maxTime;
			}
			else
			{
				enabled = false;
			}
			onTimerEnded?.Invoke();
		}
		TrackTime();
	}

	void TrackTime()
	{
		int newSecond = Mathf.FloorToInt(timeLeft % 60);
		if (second != newSecond)
		{
			onSecondElapsed?.Invoke(newSecond);
			second = newSecond;
		}

		int newMinute = Mathf.FloorToInt(timeLeft / 60f);
		if (minute != newMinute)
		{
			onMinuteElapsed?.Invoke(newMinute);
			minute = newMinute;
		}
	}

	public void Reset()
	{
		timeLeft = maxTime;
		SetEnabled(true);
	}

	public void SetEnabled(bool value)
	{
		enabled = value;
	}

	public void Toggle()
	{
		enabled = !enabled;
	}

	public override string ToString()
	{
		return string.Format("{0:0}:{1:00}", minute, second);
	}
}
