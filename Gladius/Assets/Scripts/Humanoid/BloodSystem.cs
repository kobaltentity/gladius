﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BloodSystem
{
	[System.NonSerialized] public Limb parentLimb;
	[System.NonSerialized] public Humanoid parentHumanoid;

	public float maxBloodLevel;
	public float bloodLevel;
	public float bloodLoss;

	public void Start()
	{

	}

	public void Pump() // TC: May cause a stack overflow if limbs are looping
	{
		UpdateBloodSystem();

		List<Limb> limbs = parentLimb.GetConnectedLimbs();

		foreach (Limb limb in limbs)
		{
			limb.bloodSystem.Pump();
		}
	}

	void UpdateBloodSystem()
	{
		UpdateBloodLevel();
	}


	void UpdateBloodLevel()
	{
		bloodLevel = Mathf.Clamp(bloodLevel - bloodLoss,0, maxBloodLevel);
	}
}
