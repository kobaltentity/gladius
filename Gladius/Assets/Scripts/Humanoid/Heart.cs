﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : Organ
{
	[SerializeField] float BeatsPerMinute = 85;
	Timer heartTimer;

    protected override void Start()
    {
		base.Start();
		heartTimer = new Timer();
		heartTimer.maxTime = BeatsPerMinute / 60f;
		heartTimer.onTimerEnded += PumpBlood;
		heartTimer.loop = true;
		heartTimer.SetEnabled(true);
	}

	protected override void Update()
    {
		heartTimer.maxTime = 1/(BeatsPerMinute/60f);

		base.Update();
		heartTimer.Update();
	}

	void PumpBlood()
	{
		parentLimb.bloodSystem.Pump();
	}

}
