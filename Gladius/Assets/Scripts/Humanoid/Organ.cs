﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Organ : MonoBehaviour
{
	protected Limb parentLimb;

    protected virtual void Start()
    {
		parentLimb = GetComponent<Limb>();
	}

	protected virtual void Update()
    {
        
    }
}
