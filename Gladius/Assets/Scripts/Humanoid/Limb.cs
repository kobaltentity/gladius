﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Limb : MonoBehaviour
{
	[SerializeField] float health;

	[SerializeField] public Humanoid rootHumanoid;
	[SerializeField] public List<Limb> connectedLimbs;
	[SerializeField] public List<Organ> organs;
	[SerializeField] public BloodSystem bloodSystem;

	void Awake()
    {
		bloodSystem.parentLimb = this;
		bloodSystem.Start();
	}

    void Update()
    {
    }

	public List<Limb> GetConnectedLimbs()
	{
		return connectedLimbs;
	}
}
